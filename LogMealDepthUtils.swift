import ARKit

final class LogMealDepthUtils: NSObject {
    static func isDeviceAbleToCaptureDeepthData() -> Bool {
        return ARWorldTrackingConfiguration.isSupported
    }
}
