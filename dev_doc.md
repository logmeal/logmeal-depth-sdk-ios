# How to release a new version

To release a new library version follow this simple steps:

1. Make sure you have updated your library with the latest fixes and improvements.
2. Increment the version number in the \*.podspec file.
3. Commit and push your changes to the Git repository.
4. Run the following command to check for any errors in your podspec file: `pod spec lint LogMealDepth.podspec --allow-warnings`
5. Add a tag to the specific commit that contains the version. The tag must match the version number that you will indicate in the \*.podspec file.
6. If you don't have a pod registered account yet, you will need to register first using the `pod trunk register` command: `pod trunk register myemal@email.com 'My Name' --description='Personal Laptop'`
7. If everything is fine with the podspec file, execute the following command to register the new version of your library: `pod trunk push LogMealDepth.podspec --allow-warnings`

If it's your first time uploading your library to CocoaPods, you will need to register first using the pod trunk register command.

Wait for the new version of your library to be validated and published in the CocoaPods repository.

Please note that if your library has external dependencies, you should also check and update the versions of the dependencies if necessary before uploading the new version to CocoaPods.
