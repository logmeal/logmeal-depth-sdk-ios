import UIKit

class HomeController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var apiTokenTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiTokenTextField.delegate = self
        self.apiTokenTextField.attributedPlaceholder = NSAttributedString(string: "Your API token", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func goToDepthImageExample(_ sender: Any) {
        let storyboard = self.storyboard?.instantiateViewController(withIdentifier: "DepthImageExample") as! DepthImageExampleController
        storyboard.token = apiTokenTextField.text ?? ""
        self.navigationController?.pushViewController(storyboard, animated: true)
    }
    
    @IBAction func goToDepthWasteExample(_ sender: Any) {
        let storyboard = self.storyboard?.instantiateViewController(withIdentifier: "DepthWasteExample") as! DepthWasteExampleController
        storyboard.token = apiTokenTextField.text ?? ""
        self.navigationController?.pushViewController(storyboard, animated: true)
    }
}
