import UIKit
import ARKit
import LogMealDepth


class DepthWasteExampleController: UIViewController, ARSCNViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var arSceneView: ARSCNView!
    @IBOutlet weak var endpointField: UITextField!
    @IBOutlet weak var preconsumptionImageIdField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var sendApiButton: UIButton!
    @IBOutlet weak var apiTextView: UITextView!
    
    var token: String?
    private var lmRecorder = LogMealDepth.LogMealDepthRecorder();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        arSceneView.delegate = self
        
        // Show statistics such as fps and timing information
        arSceneView.showsStatistics = true
        
        endpointField.delegate = self
        endpointField.attributedPlaceholder = NSAttributedString(string: "Endpoint", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        endpointField.text = DepthWasteApiHelper.WASTE_DEPTH_URL
        
        preconsumptionImageIdField.delegate = self
        preconsumptionImageIdField.attributedPlaceholder = NSAttributedString(string: "Preconsumption Image Id", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        // save to documents
        self.lmRecorder.setSaveToDocuments(saveToDocuments: true)
        
        if (LogMealDepthUtils.isDeviceAbleToCaptureDeepthData()) {
            // Create a session configuration
            let configuration = ARWorldTrackingConfiguration()
            
            // Run the view's session
            arSceneView.session.run(configuration)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        arSceneView.session.pause()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        guard LogMealDepthUtils.isDeviceAbleToCaptureDeepthData() else { return }
        
        if !self.lmRecorder.checkIfCapturing() { return }
        
        Task {
            do {
                try await self.lmRecorder.captureFrame(arSceneViewCurrentFrame: self.arSceneView.session.currentFrame);
            }
            catch {
                debugPrint("ERROR capturing")
            }
            if (!self.lmRecorder.checkIfCapturing() && self.lmRecorder.getIsFullCapture()) {
                self.updateUIAfterAutoStop();
            }
        }
    }
    
    // MARK: button actions
    
    @IBAction func start() {
        guard LogMealDepthUtils.isDeviceAbleToCaptureDeepthData() else { return }
        do {
            try lmRecorder.startRecording();
        }
        catch {
            debugPrint("ERROR starting", error)
        }
        self.updateUIForCapturing();
    }
    
    @IBAction func stop() {
        guard LogMealDepthUtils.isDeviceAbleToCaptureDeepthData() else { return }
        Task {
            do {
                let _ = try await lmRecorder.stopRecording();
            }
            catch {
                debugPrint("ERROR stopping", error)
            }
        }
        self.updateUIAfterStopping();
    }
    
    
    @IBAction func sendToApi() {
        debugPrint("SEND TO SERVER")
        let separator: String = "\r\n========================\r\n"
        
        DispatchQueue.main.async {
            self.apiTextView.text = "Sending..."
            self.sendApiButton.isEnabled = false
        }
        
        do {
            let requestToken: String = self.token ?? ""
            let preconsumptionId: String = preconsumptionImageIdField.text ?? ""
            let customUrl: String = endpointField.text ?? ""
            
            let parsedData = try self.lmRecorder.getLogMealDepthData().getParsedData()
            let apiSender = DepthWasteApiHelper(customUrl: customUrl)
            
            apiSender.sendRequest(authToken: requestToken,
                                  preconsumptionId: preconsumptionId,
                                  imagePath: parsedData.getImage(),
                                  sequencePath: parsedData.getSequence(),
                                  camFocalLength: parsedData.getCamFocalLength(),
                                  camPrincipalPoint: parsedData.getCamPrincipalPoint(),
                                  mainRgbIdx: parsedData.getMainRgbIdx(),
                                  cameraPose: parsedData.getCameraPose(),
                                  version: parsedData.getVersion())
            .responseString { response in
                
                debugPrint("RESPONSE")
                
                var apiText: String
                
                switch response.result {
                case .success:
                    let statusCode: Int = response.response?.statusCode ?? -1
                    if !(200...299).contains(statusCode) {
                        apiText = "ERROR: API respond with error \(separator) Status Code: \(statusCode) \(separator) \(response.value ?? "")"
                    } else {
                        
                        apiText = "SUCCESS \(separator) Status Code: \(statusCode) \(separator)"
                        
                        if let responseData = response.value!.data(using: .utf8) {
                            var jsonObject: NSDictionary
                            do {
                                jsonObject = try JSONSerialization.jsonObject(with: responseData, options: []) as! NSDictionary
                                debugPrint(jsonObject)
                                apiText += "WasteImageID: \(jsonObject["image_id_waste"] ?? "-") \(separator) \(jsonObject)"
                            } catch {
                                apiText += "Error parsing response"
                            }
                        }
                        else {
                            apiText += "Error parsing response"
                        }
                    }
                case let .failure(error):
                    apiText = "ERROR: Could not send request \(separator) \(error)"
                }
                
                DispatchQueue.main.async {
                    self.sendApiButton.isEnabled = true
                    self.apiTextView.text = apiText
                }
            }
        }
        catch {
            debugPrint("ERROR SENDING")
            DispatchQueue.main.async {
                self.apiTextView.text = "ERROR: Could not send request \(separator) UNKNOWN ERROR"
                self.sendApiButton.isEnabled = false
            }
        }
    }
    
    // MARK: UI updates
    
    func updateUIForCapturing() {
        DispatchQueue.main.async {
            self.startButton.isEnabled = false
            self.sendApiButton.isEnabled = false
            self.stopButton.isEnabled = true
            self.apiTextView.text = "Capturing..."
        }
    }
    
    func updateUIAfterAutoStop() {
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.stopButton.isEnabled = false
            self.sendApiButton.isEnabled = true
            self.apiTextView.text = "AUTO-STOPPED capture, send or start again"
        }
    }
    
    func updateUIAfterStopping() {
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.stopButton.isEnabled = false
            
            if (self.lmRecorder.isCaptureSuccessfull()) {
                self.sendApiButton.isEnabled = true
                self.apiTextView.text = "Capture stopped by user. Send or start again"
            }
            else {
                self.sendApiButton.isEnabled = false
                self.apiTextView.text = "Capture stopped by user. MISSING needed info to send to API"
            }
        }
    }
}
