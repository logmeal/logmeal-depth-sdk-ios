import Foundation
import Alamofire

public class DepthImageApiHelper{
    
    public static let IMAGE_DEPTH_URL = "https://api.logmeal.es/v2/image/segmentation/complete/quantity"
    
    private var customUrl: String?
    
    public init(){}
    
    public init(customUrl: String){
        self.customUrl = customUrl
    }
    
    public func sendRequest(
        authToken: String,
        imagePath: String,
        sequencePath: String,
        camFocalLength: String,
        camPrincipalPoint: String,
        mainRgbIdx: String,
        cameraPose: String,
        version: String
    ) -> UploadRequest {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(authToken)",
            "Accept": "application/json"
        ]
        
        let endpointUrl = self.customUrl ?? DepthImageApiHelper.IMAGE_DEPTH_URL
        
        return AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(URL(string: imagePath)!, withName: "image")
            multipartFormData.append(URL(string: sequencePath)!, withName: "sequence")
            
            multipartFormData.append(Data(camFocalLength.utf8), withName: "cam_focal_length")
            multipartFormData.append(Data(camPrincipalPoint.utf8), withName: "cam_principal_point")
            multipartFormData.append(Data(mainRgbIdx.utf8), withName: "main_rgb_idx")
            multipartFormData.append(Data(cameraPose.utf8), withName: "camera_pose")
            multipartFormData.append(Data(version.utf8), withName: "version")
        }, to: endpointUrl, headers: headers)
    }
    
    public func getCustomUrl() -> String? {
        return self.customUrl
    }
    
    public func setCustomUrl(customUrl: String?) {
        self.customUrl = customUrl
    }
}
