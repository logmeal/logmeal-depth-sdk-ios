# LogMealDepth

[![CI Status](https://img.shields.io/travis/edudoonamis/LogMealDepth.svg?style=flat)](https://travis-ci.org/edudoonamis/LogMealDepth)
[![Version](https://img.shields.io/cocoapods/v/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)
[![License](https://img.shields.io/cocoapods/l/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)
[![Platform](https://img.shields.io/cocoapods/p/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)

## Requirements

- iOS 14.0+
- Xcode 13+
- Swift 5.0+

## Installation

LogMealDepth is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

`pod 'LogMealDepth'`

Then install your pods form the comand line, running the following comand form the podfile directory:

`pod install --repo-update`

## Workflow

This SDK provides a capture mode that allows obtaining data that can be used for the API.
The SDK starts a capture process and will inform you when it is finished. Once finished, it generates all the needed data to call the API.

## Usage

The **LogMealDepth** library is a tool developed based on ARKit. Therefore, before using the methods in LogMealDepth, please ensure that you have imported the **ARKit** library.

```swift
import ARKit
import LogMealDepth
```

### ¿How it works?

Before calling the **LogMealDepth** method, please ensure that the class inherits from the **ARSCNViewDelegate** class under ARKit. Typically, the ARSCNViewDelegate class provides a series of overridden methods related to the view, which can optimize the implementation of AR experiences and depth perception features. Make sure your class inherits from this class to properly handle the lifecycle and events of the AR view

Among these methods, we need to additionally use

```swift
func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval).

```

Please ensure that your program has correctly overridden this method, like:

```swift
class LogMealDepthExampleController: UIViewController.ARSCNViewDelegate,  {
    var arSceneView: ARSCNView!
    override func viewDidLoad() {
        super.viewDidLoad()
        arSceneView.delegate = self
    }

    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval);
}

```

### Add atribute to class

Define a LogMealDepth attribute:

```swift
private var lmRecorder = LogMealDepth.LogMealDepthRecorder();
```

This class must be called before using the LogMealDepth library.

### Add recoder on `renderer`

```swift
func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
    try await lmRecorder.captureFrame(arSceneViewCurrentFrame: self.arSceneView.session.currentFrame);
}
```

Within the rendering process, the LogMealDepth class is employed to efficiently capture valuable information. The captureFrame method is utilized, designed to take snapshots of each frame from the camera and store the associated data.

### Obtain result

```swift
let logMealDepthData = self.lmRecorder.getLogMealDepthData()
let parsedData = self.lmRecorder.getLogMealDepthData().getParsedData()
```

To retrieve data, you have two methods at your disposal.

- `getLogMealDepthData` allows you to obtain all the relevant data captured during rendering.

- `getParsedData` is designed to transform the entire dataset into a format suitable for API usage. This facilitates the seamless and efficient transmission of data to the server, ensuring a smoother and more convenient process.

### Add posibility to stop recording

In the case that the user is given the possibility of cutting the capture, `stopRecording()` must be used:

```swift
@IBAction func stop() {
    try await lmRecorder.stopRecording();
}
```

When stopRecording is called, it will automatically save and generate a data file.

### Important classes

#### LogMealDepthRecorder

The `LogMealDepthRecorder` class provides all the methods needed for recording and their status and results.

```swift
public func startRecording()
public func stopRecording()
public func captureFrame(arSceneViewCurrentFrame: ARFrame?)
public func checkIfCapturing() -> Bool
public func isCaptureSuccessfull() -> Bool
public func getIsFullCapture() -> Bool
public func getLogMealDepthData() -> LogMealDepthData
public func getSaveToDocuments() -> Bool
public func setSaveToDocuments(saveToDocuments: Bool)
```

#### LogMealDepthUtils

The `LogMealDepthUtils` class provides a simple method to verify whether the device can use the library:

```swift
public static func isDeviceAbleToCaptureDeepthData() -> Bool
```

##### Example

```swift
func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if (LogMealDepthUtils.isDeviceAbleToCaptureDeepthData()) {
            try await lmRecorder.captureFrame(arSceneViewCurrentFrame: self.arSceneView.session.currentFrame);
    }
}

```

#### LogMealDepthData

It's purpose is to hold and modify all the data from the captured frame, this data should be used with the LogMeal API. It has the detailed data in the following methods:

```swift
public func generateApiInput() -> [String: Any]
public func getParsedData() throws -> LogMealDepthParsedData
public func getCamFocalLength() -> CGPoint
public func setCamFocalLength(_ newFocalLength: CGPoint)
public func getCamPrincipalPoint() -> CGPoint
public func setCamPrincipalPoint(_ newPrincipalPoint: CGPoint)
public func getCameraPoses() -> [String: [Float]]
public func setCameraPoses(_ newCameraPoses: [String: [Float]])
public func setCameraPosesWithIndex(_ index: String, _ cameraPoses: [Float])
public func getCameraPosesStr() throws -> String
public func getMainRGBIndex() -> Int
public func setMainRGBIndex(_ newIndex: Int)
public func getMainRGBPath() -> URL
public func setMainRGBPath(_ newPath: URL)
public func getImageObjects() -> [UIImage]
public func pushImageObject(_ newImageObject: UIImage)
public func getVideoPath() -> URL
public func setVideoPath(_ newVideoPath: URL)
public func getVersion() -> String
```

#### LogMealDepthParsedData

It's purpose is to hold all the data from the captured frame, this data should be used with the LogMeal API. It has the detailed data in the following methods:

```swift
public func asDict() -> NSDictionary
public func getImage() -> String
public func getSequence() -> String
public func getCamFocalLength() -> String
public func getCamPrincipalPoint() -> String
public func getMainRgbIdx() -> String
public func getCameraPose() -> String
public func getVersion() -> String
```

LogMeal API's Depth Endpoint requires the following data:

- Field=image : Path to image file: `getImage()`
- Field=sequence : Path to sequence file: `getSequence()`
- Field=main_rgb_idx : Index of main RGB image: `getMainRgbIdx()`
- Field=cam_focal_length : Depth Camera focal length `getCamFocalLength()`
- Field=cam_principal_point : Depth Camera principal point `getCamPrincipalPoint()`
- Field=camera_pose : Depth Camera poses `getCameraPose()`
- Field=depth_version: `getVersion()`

### Example project

The library comes with an example project that can be taken as an example. This application is in the example folder.
To run the project, you have to follow these simple steps:

    1. Clone or download the library repository.
    2. Open a terminal and go to the example folder.
    3. Run `pod install` to install the dependencies of the example project.
    4. Navigate to the example directory using Finder and open the *.xcworkspace file generated from CocoaPods.
    5. Run the project.

## Author

LogMeal © 2024. All Rights Reserved. | contact@logmeal.es
