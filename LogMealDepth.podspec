#
# Be sure to run `pod lib lint LogMealDepth.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LogMealDepth'
  s.version          = '2.1.0'
  s.summary          = 'A library to capture depth images to be used with LogMeal services.'

  s.description      = <<-DESC
This library aims to to implement a means to capture depth images and the needed metadata so that it can be properly used versus the logMeal services in order to get the meal info.
                       DESC

  s.homepage         = 'https://gitlab.com/logmeal/logmeal-depth-sdk-ios'
  s.license          = { :type => 'propietary', :file => 'LICENSE' }
  s.author           = { 'AIGecko Technologies SL' => 'contact@logmeal.es' }
  s.source           = { :git => 'https://gitlab.com/logmeal/logmeal-depth-sdk-ios.git', :tag => '2.1.0' }
  s.social_media_url = 'https://x.com/logmeal_ai'
  
  s.swift_version = '5.7'

  s.ios.deployment_target = '14.0'

  s.source_files = 'LogMealDepth/Classes/**/*.swift'
  
  #s.resource_bundles = {
  #   'LogMealDepth' => ['LogMealDepth/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}']
  #  }
  
  s.frameworks = 'UIKit', 'AVFoundation', 'ARKit'
end
