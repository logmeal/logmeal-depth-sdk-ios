import UIKit
import simd

class LMDepthCapture {
    var index: Int
    var angle: Float
    var rangeMin: Float
    var rangeMax: Float
    var width: Float
    var height: Float
    var image: UIImage?
    var cameraPose: simd_float4x4?
    var fx: Float
    var fy: Float
    var cx: Float
    var cy: Float
    
    init(index: Int,
         angle: Float,
         rangeMin: Float,
         rangeMax: Float,
         width: Float = 0.0,
         height: Float = 0.0,
         image: UIImage? = nil,
         cameraPose: simd_float4x4? = nil,
         fx: Float = 0.0,
         fy: Float = 0.0,
         cx: Float = 0.0,
         cy: Float = 0.0) {
        self.index = index
        self.angle = angle
        self.rangeMin = rangeMin
        self.rangeMax = rangeMax
        self.width = width
        self.height = height
        self.image = image
        self.cameraPose = cameraPose
        self.fx = fx
        self.fy = fy
        self.cx = cx
        self.cy = cy
    }
}
