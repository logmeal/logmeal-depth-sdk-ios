import Foundation
public class LogMealDepthParsedData {
    
    private var image: String
    private var sequence: String
    private var cam_focal_length: String
    private var cam_principal_point: String
    private var main_rgb_idx: String
    private var camera_pose: String
    private var version: String
    
    public init(
        image: String,
        sequence: String,
        cam_focal_length: String,
        cam_principal_point: String,
        main_rgb_idx: String,
        camera_pose: String,
        version: String) {
            
            self.image = image
            self.sequence = sequence
            self.cam_focal_length = cam_focal_length
            self.cam_principal_point = cam_principal_point
            self.main_rgb_idx = main_rgb_idx
            self.camera_pose = camera_pose
            self.version = version
        }
    
    public func asDict() -> NSDictionary {
        return [
            "image": self.image,
            "sequence": self.sequence,
            "cam_focal_length": self.cam_focal_length,
            "cam_principal_point": self.cam_principal_point,
            "main_rgb_idx": self.main_rgb_idx,
            "camera_pose": self.camera_pose,
            "version": self.version
        ]
    }
    
    public func getImage() -> String {
        return self.image
    }
    
    public func getSequence() -> String {
        return self.sequence
    }
    
    public func getCamFocalLength() -> String {
        return self.cam_focal_length
    }
    
    public func getCamPrincipalPoint() -> String {
        return self.cam_principal_point
    }
    
    public func getMainRgbIdx() -> String {
        return self.main_rgb_idx
    }
    
    public func getCameraPose() -> String {
        return self.camera_pose
    }
    
    public func getVersion() -> String {
        return self.version
    }
}
