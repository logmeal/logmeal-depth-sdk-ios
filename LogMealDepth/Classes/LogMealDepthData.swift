import Foundation
import UIKit

public class LogMealDepthData {
    
    public static var IOS_VERSION = "ios"
    
    private var cam_focal_length: CGPoint
    private var cam_principal_point: CGPoint
    private var camera_poses: [String: [Float]]
    private var main_rgb_idx: Int;
    private var main_rgb_path: URL;
    private var image_objects: [UIImage]
    private var video_path: URL;
    private var version: String;
    
    public init(cam_focal_length: CGPoint = .zero,
                cam_principal_point: CGPoint = .zero,
                camera_poses: [String: [Float]] = [:],
                main_rgb_idx: Int = 0,
                main_rgb_path: URL = URL(fileURLWithPath: ""),
                image_objects: [UIImage] = [],
                video_path: URL = URL(fileURLWithPath: ""),
                version: String = "") {
        
        self.cam_focal_length = cam_focal_length
        self.cam_principal_point = cam_principal_point
        self.camera_poses = camera_poses
        self.main_rgb_idx = main_rgb_idx
        self.main_rgb_path = main_rgb_path
        self.image_objects = image_objects
        self.video_path = video_path
        self.version = LogMealDepthData.IOS_VERSION
    }
    
    public func generateApiInput() -> [String: Any]{
        return [
            "cam_focal_length": ["x": self.cam_focal_length.x, "y": self.cam_focal_length.y],
            "cam_principal_point": ["x": self.cam_principal_point.x, "y": self.cam_principal_point.y],
            "main_rgb_idx": self.getMainRGBIndex(),
            "camera_poses": self.getCameraPoses(),
            "version": "ios",
        ]
    }
    
    public func getParsedData() throws -> LogMealDepthParsedData {
        return LogMealDepthParsedData(
            image: self.main_rgb_path.absoluteString,
            sequence: self.video_path.absoluteString,
            cam_focal_length: "{\"x\":\(self.cam_focal_length.x),\"y\":\(self.cam_focal_length.y)}",
            cam_principal_point: "{\"x\":\(self.cam_principal_point.x),\"y\":\(self.cam_principal_point.y)}",
            main_rgb_idx: String(main_rgb_idx),
            camera_pose: try getCameraPosesStr(),
            version: self.version
        )
    }
    
    public func getCamFocalLength() -> CGPoint {
        return self.cam_focal_length
    }
    
    public func setCamFocalLength(_ newFocalLength: CGPoint) {
        self.cam_focal_length = newFocalLength
    }
    
    public func getCamPrincipalPoint() -> CGPoint {
        return self.cam_principal_point
    }
    
    public func setCamPrincipalPoint(_ newPrincipalPoint: CGPoint) {
        self.cam_principal_point = newPrincipalPoint
    }
    
    public func getCameraPoses() -> [String: [Float]] {
        return self.camera_poses;
    }
    
    public func setCameraPoses(_ newCameraPoses: [String: [Float]]) {
        self.camera_poses = newCameraPoses
    }
    
    public func setCameraPosesWithIndex(_ index: String, _ cameraPoses: [Float]) {
        self.camera_poses[index] = cameraPoses;
    }
    
    public func getCameraPosesStr() throws -> String {
        let dataCameraPoses = try JSONSerialization.data(withJSONObject: self.camera_poses, options: [.sortedKeys])
        return String(data: dataCameraPoses, encoding: .utf8)!
    }
    
    public func getMainRGBIndex() -> Int {
        return self.main_rgb_idx
    }
    
    public func setMainRGBIndex(_ newIndex: Int) {
        self.main_rgb_idx = newIndex
    }
    
    public func getMainRGBPath() -> URL {
        return self.main_rgb_path
    }
    
    public func setMainRGBPath(_ newPath: URL) {
        self.main_rgb_path = newPath
    }
    
    public func getImageObjects() -> [UIImage] {
        return self.image_objects
    }
    
    public func pushImageObject(_ newImageObject: UIImage) {
        self.image_objects.append(newImageObject)
    }
    
    public func getVideoPath() -> URL {
        return video_path
    }
    
    public func setVideoPath(_ newVideoPath: URL) {
        video_path = newVideoPath
    }
    
    public func getVersion() -> String {
        return self.version
    }
    
    public func toDict() -> NSDictionary {
        return [
            "camFocalLength": ["x": self.cam_focal_length.x, "y": self.cam_focal_length.y],
            "camPrincipalPoint": ["x": self.cam_principal_point.x, "y": self.cam_principal_point.y],
            "cameraPoses": self.camera_poses,
            "mainRgbIdx": self.main_rgb_idx,
            "mainRgbPath": self.main_rgb_path.absoluteString,
            "videoPath": self.video_path.absoluteString,
            "version": self.version,
        ]
    }
}
