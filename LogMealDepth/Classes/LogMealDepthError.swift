public enum LogMealDepthError: Error {
    case creatingDocumentOutputPath
    case cantGetDataFromUnsuccessfullCapture
}
