import ARKit

public final class LogMealDepthUtils: NSObject {
    public static func isDeviceAbleToCaptureDeepthData() -> Bool {
        // Check if ARWorldTrackingConfiguration is supported
        guard ARWorldTrackingConfiguration.isSupported else { return false }
        
        return !ARWorldTrackingConfiguration.supportedVideoFormats.isEmpty
    }
}
