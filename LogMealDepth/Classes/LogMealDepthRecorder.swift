import Foundation
import UIKit
import ARKit
import simd
import Accelerate
import GLKit
import ImageIO
import UniformTypeIdentifiers

public class LogMealDepthRecorder{
    
    private let UPPER_ANGLE_LIMIT: Float = 180
    private let MAX_ANGLE: Float = 150
    private let MIN_ANGLE: Float = 30
    private let MIN_CAPTURES: Int = 50
    private let OPTIMAL_CAPTURES: Float = 100
    private let MAIN_FRAME_ANGLE: Float = 90
    private let MAIN_FRAME_TOLERANCE: Float = 10
    private let FULL_CAPTURE_CONFIRMS_TO_AUTOSTOP: Int = 8
    private let FRAMES_TO_SKIP: Int = 12
    private let NEXT_ANGLE_TOLERANCE: Float = 15
    
    private var skippedFrames: Int = 0
    private var isCapturing = false
    private var isFullCapture = false
    private var fullCaptureConfirmCount = 0
    private var isFirstFrame = true
    private var captures: [LMDepthCapture] = []
    private var cameras_acquisition: [simd_float4x4] = []
    private var center_update: [(Float,[Float])] = []
    private var center_update_weighted: [Float] = []
    private var outputPath: URL = URL(fileURLWithPath: NSTemporaryDirectory())
    private var saveToDocuments = false
    private var lmDepthData = LogMealDepthData()
    private var lastCapturedAngle: Float? = nil
    
    public init(){
        self.initCaptureVideo()
    }
    
    private func initCaptureVideo(){
        self.captures = self.initCaptures()
        self.cameras_acquisition = []
        self.center_update = [self.initializeSceneCenter()]
        self.center_update_weighted = []
        self.isFirstFrame = true
        self.isFullCapture = false
        self.fullCaptureConfirmCount = 0
        self.skippedFrames = 0
        self.lastCapturedAngle = nil
    }
    
    public func startRecording() throws {
        self.isCapturing = true;
        try self.initOutputPath()
        self.initCaptureVideo()
    }
    
    public func stopRecording() async throws {
        self.isCapturing = false
        self.isFirstFrame = true
        try await self.processCapturedData()
    }
    
    public func captureFrame(arSceneViewCurrentFrame: ARFrame?) async throws {
        
        if !self.isCapturing {
            return
        }
        
        let saveGroup = DispatchGroup()
        
        if let frame = arSceneViewCurrentFrame {
            
            let cameraPose = frame.camera.transform
            self.cameras_acquisition.append(cameraPose)
            
            if !self.isFirstFrame {
                
                self.center_update.append(self.updateSceneCenterFromCams(trajectory:self.cameras_acquisition, center: self.center_update.last!))
                
                let curr_center = self.applyWeightSceneCenter(self.center_update.last!)
                
                self.center_update_weighted.append(contentsOf:curr_center)
                
                let relativeAngle = self.getAngleWithRespectToFirstCam(
                    cam0proj: [Float(self.cameras_acquisition[0][3].x), Float(curr_center[1]), Float(self.cameras_acquisition[0][3].z)],
                    center: curr_center,
                    arcoreCamsMatrix: self.cameras_acquisition)
                
                
                if let angle = relativeAngle.last, !angle.isNaN {
                    
                    var dictIndex = -1;
                    if angle < self.MIN_ANGLE {
                        dictIndex = Int(angle)
                    } else if angle >= self.MIN_ANGLE && angle <= self.MAX_ANGLE {
                        dictIndex = Int(self.MIN_ANGLE + (angle  - self.MIN_ANGLE) / ( (self.MAX_ANGLE - self.MIN_ANGLE) / self.OPTIMAL_CAPTURES ) )
                    }
                    else {
                        // if angle > self.MAX_ANGLE
                        dictIndex = Int((angle - self.MAX_ANGLE) + (self.MIN_ANGLE + self.OPTIMAL_CAPTURES))
                    }
                    
                    if self.skippedFrames < self.FRAMES_TO_SKIP {
                        self.skippedFrames += 1
                        return
                    }
                    
                    if self.checkRange(index: dictIndex, angle: angle) {
                        
                        if ((self.lastCapturedAngle != nil) &&
                            (abs(angle - self.lastCapturedAngle!) > self.NEXT_ANGLE_TOLERANCE)) {
                            debugPrint("Skipped angle: \(angle) - last: \(self.lastCapturedAngle!)")
                            return
                        }
                        
                        self.lastCapturedAngle = angle
                       
                        
                        saveGroup.enter()
                        
                        Task {
                            let ciImage = CIImage(cvPixelBuffer: frame.capturedImage)
                            
                            let originalWidth = CVPixelBufferGetWidth(frame.capturedImage)
                            let originalHeight = CVPixelBufferGetHeight(frame.capturedImage)
                            let aspectRatio = Float(originalWidth) / Float(originalHeight)
                            
                            let imgTargetWidth: Float = 800.0
                            let imgTargetHeight = imgTargetWidth / aspectRatio
                            
                            let paramTargetWidth: Float = 640.0
                            let paramTargetHeight = paramTargetWidth / aspectRatio
                            
                            let resizedCIImage = ciImage.transformed(by: CGAffineTransform(scaleX: CGFloat(imgTargetWidth / Float(originalWidth)), y: CGFloat(imgTargetHeight / Float(originalHeight))))
                            
                            
                            if let cgImage = CIContext(options: nil).createCGImage(resizedCIImage, from: resizedCIImage.extent) {
                                
                                let image = UIImage(cgImage: cgImage, scale: 1.0, orientation: .up)
                                let focalLengthX = Float(frame.camera.intrinsics.columns.0.x) * paramTargetWidth / Float(originalWidth)
                                let focalLengthY = Float(frame.camera.intrinsics.columns.1.y) * paramTargetHeight / Float(originalHeight)
                                let principalPointX = Float(frame.camera.intrinsics.columns.2.x)  * paramTargetWidth / Float(originalWidth)
                                let principalPointY = Float(frame.camera.intrinsics.columns.2.y) * paramTargetHeight / Float(originalHeight)
                                
                                let capture = self.captures[dictIndex]
                                capture.index = dictIndex
                                capture.angle = angle
                                capture.width = imgTargetWidth
                                capture.height = imgTargetHeight
                                capture.image = image
                                capture.cameraPose = cameraPose
                                capture.fx = focalLengthX
                                capture.fy = focalLengthY
                                capture.cx = principalPointX
                                capture.cy = principalPointY
                                self.captures[dictIndex] = capture
                            }
                            saveGroup.leave()
                        }
                        if self.autoStopCaptures(captures: self.captures){
                            debugPrint("auto Stop Capture")
                            try await self.stopRecording();
                        }
                    }
                }
            }else{
                self.isFirstFrame = false;
            }
        }
        
    }
    
    public func isCaptureSuccessfull() -> Bool{
        if self.isFullCapture{
            return true
        }
        
        var validCaptureCount = 0
        var valid90DegreeCaptureCount = 0
        
        let desiredCapturesStartIdx = Int(self.MIN_ANGLE)
        let desiredCapturesEndIdx = Int(self.MIN_ANGLE+OPTIMAL_CAPTURES-1)
        
        // count images between MIN_ANGLE and MAX_ANGLE while checking main frame
        for idx in desiredCapturesStartIdx ... desiredCapturesEndIdx {
            if self.captures[idx].image == nil || self.captures[idx].cameraPose == nil {
                validCaptureCount += 1
                if (abs(MAIN_FRAME_ANGLE - self.captures[idx].angle) <= MAIN_FRAME_TOLERANCE) {
                    valid90DegreeCaptureCount += 1;
                }
            }
        }
        
        debugPrint("Captures \(validCaptureCount) - MIN: \(self.MIN_CAPTURES) - Main # \(valid90DegreeCaptureCount)")
        
        return validCaptureCount >= self.MIN_CAPTURES && valid90DegreeCaptureCount >= 1
    }
    
    public func checkIfCapturing() -> Bool {
        return self.isCapturing
    }
    
    public func getLogMealDepthData() -> LogMealDepthData {
        return self.lmDepthData
    }
    
    public func getIsFullCapture() -> Bool {
        return self.isFullCapture
    }
    
    public func processCapturedData() async throws {
        self.lmDepthData = LogMealDepthData();
        
        let saveGroup = DispatchGroup()
        
        var index = 0;
        let middleAngle = self.MAIN_FRAME_ANGLE;
        var closeTo90Degrees = self.MAIN_FRAME_ANGLE;
        
        for capture in captures {
            if capture.image != nil && capture.cameraPose != nil, let imageObject = capture.image {
                self.lmDepthData.pushImageObject(imageObject)
                
                if let cameraPosObject = capture.cameraPose {
                    let cameraPoseArray = matrixToArray(matrix: cameraPosObject)
                    
                    var info = ""
                    info += "width:" + "\(capture.width)\n"
                    info += "height:" + "\(capture.height)\n"
                    info += "fx:" + "\(capture.fx)\n"
                    info += "fy:" + "\(capture.fy)\n"
                    info += "cx:" + "\(capture.cx)\n"
                    info += "cy:" + "\(capture.cy)\n"
                    info += "modelMatrix:" + "\n"
                    
                    for i in 0..<16 {
                        info += ":\(cameraPoseArray[i])\n"
                    }
                    
                    let strIndex = String(format: "%04d", index)
                    
                    await self.saveToFilesSingleTest(image: imageObject, info: info, filename: strIndex)
                    
                    self.lmDepthData.setCameraPosesWithIndex(strIndex, cameraPoseArray)
                    
                    let angleDifference = abs(capture.angle - middleAngle)
                    if  angleDifference < closeTo90Degrees {
                        self.lmDepthData.setCamFocalLength(CGPoint(x: Double(capture.fx),
                                                                   y: Double(capture.fy)))
                        self.lmDepthData.setCamPrincipalPoint(CGPoint(x: Double(capture.cx),
                                                                      y: Double(capture.cy)))
                        self.lmDepthData.setMainRGBIndex(index)
                        self.lmDepthData.setMainRGBPath(self.outputPath.appendingPathComponent("\(strIndex).jpeg"))
                        
                        closeTo90Degrees = angleDifference
                    }
                    index += 1
                }
            }
        }
        
        // Add EXIF metadata to the main RGB image
        let mainImagePath = self.lmDepthData.getMainRGBPath()
        debugPrint("MainRGB => index:\(self.lmDepthData.getMainRGBIndex()) - path:\(mainImagePath.path)")
        if FileManager.default.fileExists(atPath: mainImagePath.path) {
            try addExifMetadataToImage(at: mainImagePath, orientation: .right) // Adjust the orientation as needed
        }
        
        let jsonData = try JSONSerialization.data(withJSONObject: self.lmDepthData.generateApiInput(), options: [.prettyPrinted, .sortedKeys])
        if let jsonString = String(data: jsonData, encoding: .utf8) {
            saveGroup.enter()
            await self.saveToFilesApiOutput(info:jsonString, filename: "apiinput.json");
            saveGroup.leave()
        }
        
        
        self.lmDepthData.setVideoPath(self.outputPath.appendingPathComponent("output_video.avi"))
        saveGroup.enter()
        try await self.createVideoFromImages(images: self.lmDepthData.getImageObjects(), outputURL: self.lmDepthData.getVideoPath())
        saveGroup.leave()
    }
    
    public func getSaveToDocuments() -> Bool {
        return self.saveToDocuments
    }
    
    public func setSaveToDocuments(saveToDocuments: Bool) {
        self.saveToDocuments = saveToDocuments
    }
    
    private func initCaptures() -> [LMDepthCapture] {
        let stepSize = (self.MAX_ANGLE - self.MIN_ANGLE) / self.OPTIMAL_CAPTURES
        var captures = [LMDepthCapture]()
        
        // Initialize extra captures: 0 29
        for angle in 0 ..< Int(self.MIN_ANGLE) {
            let capture = LMDepthCapture(index: angle,
                                         angle: Float(angle),
                                         rangeMin: Float(angle),
                                         rangeMax: Float(angle) + 0.9)
            captures.append(capture)
        }
        
        // Main captures
        for index in 0 ..< Int(self.OPTIMAL_CAPTURES) {
            let angle = self.MIN_ANGLE + Float(index) * stepSize
            let capture: LMDepthCapture = LMDepthCapture(index: index, angle: angle, rangeMin: angle, rangeMax: angle + stepSize);
            captures.append(capture)
        }
        
        // Extra Range: 150.01 150.9
        captures.append(
            LMDepthCapture(
                index: captures.count,
                angle: self.MAX_ANGLE,
                rangeMin: self.MAX_ANGLE+0.01,
                rangeMax: self.MIN_ANGLE + 0.9)
        )
        
        for angle in Int(self.MAX_ANGLE+1) ..< Int(self.UPPER_ANGLE_LIMIT) {
            let capture: LMDepthCapture = LMDepthCapture(
                index: captures.count,
                angle: Float(angle),
                rangeMin: Float(angle),
                rangeMax: Float(angle) + 0.9)
            captures.append(capture)
        }
        
        return captures;
    }
    
    private func checkRange(index: Int, angle: Float) -> Bool {
        guard index >= 0 && index < self.captures.count else {
            return false
        }
        
        debugPrint("checkRange => index:\(index) - angle:\(angle) - min:\(self.captures[index].rangeMin) - max:\(self.captures[index].rangeMax)")
        
        return (angle >= self.captures[index].rangeMin) && (angle <= self.captures[index].rangeMax)
    }
    
    private func autoStopCaptures(captures: [LMDepthCapture]) -> Bool {
        let desiredCapturesStartIdx = Int(self.MIN_ANGLE)
        let desiredCapturesEndIdx = Int(self.MIN_ANGLE+OPTIMAL_CAPTURES-1)
        
        for idx in desiredCapturesStartIdx ... desiredCapturesEndIdx {
            if self.captures[idx].image == nil || self.captures[idx].cameraPose == nil {
                self.isFullCapture = false
                return false
            }
        }
        
        self.isFullCapture = true
        self.fullCaptureConfirmCount += 1
        return self.fullCaptureConfirmCount == self.FULL_CAPTURE_CONFIRMS_TO_AUTOSTOP
    }
    
    
    
    private func saveToFiles(image: UIImage, cameraPose: simd_float4x4) async {
        let time = getTimeStr()
        
        do {
            try await savePic(pic: image, filename: "\(time).jpeg", folder: "")
            
            let cameraPoseArray = matrixToArray(matrix: cameraPose)
            if let jsonData = try? JSONSerialization.data(withJSONObject: cameraPoseArray, options: .prettyPrinted) {
                try await saveFile(content: String(data: jsonData, encoding: .utf8) ?? "", filename: "\(time).json", folder: "")
            }
        } catch {
            debugPrint("Error saving files: \(error)")
        }
    }
    
    private func saveToFilesSingleTest(image: UIImage, info: String, filename: String) async {
        
        do {
            try await savePic(pic: image, filename: "\(filename).jpeg", folder: "")
            try await saveFile(content: info, filename: "\(filename).json", folder: "")
        } catch {
            debugPrint("Error saving files: \(error)")
        }
    }
    
    
    private func saveToFilesApiOutput(info: String, filename: String) async {
        do {
            try await saveFile(content: info, filename: filename, folder: "")
        } catch {
            debugPrint("Error saving files: \(error)")
        }
    }
    
    
    private func matrixToArray(matrix: simd_float4x4) -> [Float] {
        var result: [Float] = []
        for row in 0..<4 {
            for col in 0..<4 {
                result.append(matrix[row][col])
            }
        }
        return result
    }
    
    /// Save file to a directory.
    private func saveFile(content: String, filename: String, folder: String) async throws -> () {
        let url = self.outputPath.appendingPathComponent(folder, isDirectory: true).appendingPathComponent(filename)
        try content.write(to: url, atomically: true, encoding: .utf8)
    }
    
    /// Save jpeg to a directory.
    private func savePic(pic: UIImage , filename: String, folder: String) async throws -> () {
        let url = self.outputPath.appendingPathComponent(filename)
        try pic.jpegData(compressionQuality: 0)?.write(to: url)
    }
    
    private func initOutputPath() throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy_MM_dd_HHmmss"
        let currentDateTime = Date()
        let outFolder = dateFormatter.string(from: currentDateTime)
        let baseFolder = self.saveToDocuments
        ? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        : URL(fileURLWithPath: NSTemporaryDirectory())
        let folderURL = baseFolder.appendingPathComponent(outFolder)
        
        // Check if the directory already exists
        var isDirectory: ObjCBool = false
        if FileManager.default.fileExists(atPath: folderURL.path, isDirectory: &isDirectory) {
            if !isDirectory.boolValue {
                // A file with the same name exists
                throw LogMealDepthError.creatingDocumentOutputPath
            }
        } else {
            try FileManager.default.createDirectory(at: folderURL, withIntermediateDirectories: true, attributes: nil)
            self.outputPath = folderURL
        }
    }
    
    private func getTimeStr() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd_HHmmss_SSS"
        return df.string(from: Date())
    }
    
    private func initializeSceneCenter() -> (Float, [Float]) {
        let totw = Float(0.0)
        let totp = [Float(0.0), Float(0.0), Float(0.0)]
        return (totw, totp)
    }
    
    
    private func findSceneCenterFromCams(trajectory: [simd_float4x4]) -> ([Float]){
        var totw = Float(0.0)
        var totp = [Float](repeating: 0.0, count: 3)
        
        for cam in trajectory {
            
            let c1_col_3 = cam.columns.3
            let c1_col_2 = cam.columns.2
            
            
            for cam2 in trajectory{
                
                let c2_col_3 = cam2.columns.3
                let c2_col_2 = cam2.columns.2
                
                let c1Col3Array = simd_float3(c1_col_3.x, c1_col_3.y, c1_col_3.z)
                let c1Col2Array = simd_float3(c1_col_2.x, c1_col_2.y, c1_col_2.z)
                let c2Col3Array = simd_float3(c2_col_3.x, c2_col_3.y, c2_col_3.z)
                let c2Col2Array = simd_float3(c2_col_2.x, c2_col_2.y, c2_col_2.z)
                let (p, w2) = closestPointTwoLines(oa: c1Col3Array, da: c1Col2Array, ob: c2Col3Array, db: c2Col2Array)
                
                if w2 > 0.00001 {
                    totp = Array(totp.enumerated().map { (index, value) in
                        return value + (p[index] * w2)
                    })
                    totw += w2
                }
            }
        }
        
        
        if totw > 0.0 {
            totp = Array(totp.enumerated().map { (index, value) in
                return value / totw
            })
        }
        
        return totp
    }
    
    
    
    private func updateSceneCenterFromCams(trajectory: [simd_float4x4], center: (Float, [Float])) -> (Float, [Float]) {
        var (totw, totp) = center
        
        guard let lastCam = trajectory.last else {
            return (totw, totp)
        }
        
        let lcCol3 = lastCam.columns.3
        let lcCol2 = lastCam.columns.2
        
        for cam in trajectory {
            let tcCol3 = cam.columns.3
            let tcCol2 = cam.columns.2
            
            let lcCol3Array = simd_float3(lcCol3.x, lcCol3.y, lcCol3.z)
            let lcCol2Array = simd_float3(lcCol2.x, lcCol2.y, lcCol2.z)
            let tcCol3Array = simd_float3(tcCol3.x, tcCol3.y, tcCol3.z)
            let tcCol2Array = simd_float3(tcCol2.x, tcCol2.y, tcCol2.z)
            let (p, w2) = closestPointTwoLines(oa: lcCol3Array, da: lcCol2Array, ob: tcCol3Array, db: tcCol2Array)
            
            if w2 > 0.00001 {
                totp = totp.enumerated().map { (index, value) in
                    return value + (p[index] * w2)
                }
                totw += w2
            }
        }
        
        return (totw, totp)
    }
    
    
    
    private func closestPointTwoLines(oa: simd_float3, da: simd_float3, ob: simd_float3, db: simd_float3) -> (simd_float3, Float) {
        let daNormalized = simd_normalize(da)
        let dbNormalized = simd_normalize(db)
        
        let c = simd_cross(daNormalized, dbNormalized)
        let denom = length(c) * length(c)
        
        
        let t = ob - oa
        var ta = simd_dot(t, simd_cross(db,c)) / (denom + 1e-10)
        var tb = simd_dot(t, simd_cross(da,c)) / (denom + 1e-10)
        
        if ta > 0 {
            ta = 0
        }
        
        if tb > 0 {
            tb = 0
        }
        
        return (simd_float3((oa + ta * da + ob + tb * db) * 0.5), denom)
    }
    
    
    
    private func applyWeightSceneCenter(_ center: (Float, [Float])) -> [Float] {
        var (totw, totp) = center
        
        if totw > 0.0 {
            totp = totp.map { $0 / totw }
        }
        
        return totp
    }
    
    
    
    private func getAngleWithRespectToFirstCam(cam0proj: [Float], center: [Float], arcoreCamsMatrix: [simd_float4x4]) -> [Float] {
        var relativeTheta = [Float]()
        
        var cam2centerVector0 = simd_float3(cam0proj[0], cam0proj[1], cam0proj[2]) -
        simd_float3(center[0], center[1], center[2])
        
        cam2centerVector0 = cam2centerVector0 / simd_length(cam2centerVector0)
        
        for camMatrix in arcoreCamsMatrix {
            var cam2centerVector = simd_float3(camMatrix[3].x, camMatrix[3].y, camMatrix[3].z) -
            simd_float3(center[0], center[1], center[2])
            
            cam2centerVector = cam2centerVector / simd_length(cam2centerVector)
            
            let theta = acos(simd_dot(cam2centerVector0, cam2centerVector)) * (180.0 / .pi)
            relativeTheta.append(theta)
        }
        
        return relativeTheta
    }
    
    
    
    private func createVideoFromImages(images: [UIImage], outputURL: URL) async throws {
        guard !images.isEmpty else {
            throw NSError(domain: "YourAppDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No images provided"])
        }
        
        let videoWriter: AVAssetWriter
        do {
            videoWriter = try AVAssetWriter(outputURL: outputURL, fileType: .mp4)
        } catch {
            throw error
        }
        
        let outputSettings: [String: Any] = [
            AVVideoCodecKey: AVVideoCodecType.h264,
            AVVideoWidthKey: 640,
            AVVideoHeightKey: 480,
            AVVideoCompressionPropertiesKey: [
                AVVideoAverageBitRateKey: 3000000,
                AVVideoProfileLevelKey: AVVideoProfileLevelH264HighAutoLevel,
                AVVideoH264EntropyModeKey: AVVideoH264EntropyModeCAVLC,
            ],
        ]
        
        let videoWriterInput = AVAssetWriterInput(mediaType: .video, outputSettings: outputSettings)
        videoWriterInput.expectsMediaDataInRealTime = false
        
        let sourceBufferAttributes: [String: Any] = [
            kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32ARGB,
            kCVPixelBufferWidthKey as String: images[0].size.width,
            kCVPixelBufferHeightKey as String: images[0].size.height,
            kCVPixelFormatOpenGLESCompatibility as String: true,
        ]
        
        let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput,
                                                                      sourcePixelBufferAttributes: sourceBufferAttributes)
        
        if videoWriter.canAdd(videoWriterInput) {
            videoWriter.add(videoWriterInput)
        } else {
            throw NSError(domain: "YourAppDomain", code: 2, userInfo: [NSLocalizedDescriptionKey: "Cannot add video writer input"])
        }
        
        videoWriter.startWriting()
        videoWriter.startSession(atSourceTime: CMTime.zero)
        
        var frameCount = 0
        let frameDuration = CMTimeMake(value: 1, timescale: 30) // Assuming 30 frames per second
        
        for image in images {
            guard videoWriter.status == .writing else {
                break
            }
            
            if let pixelBuffer = image.pixelBuffer(width: Int(image.size.width), height: Int(image.size.height)) {
                while !pixelBufferAdaptor.assetWriterInput.isReadyForMoreMediaData {
                    try await Task.sleep(nanoseconds: 1_000_000) // 1ms
                }
                
                let presentationTime = CMTimeMultiply(frameDuration, multiplier: Int32(frameCount))
                if !pixelBufferAdaptor.append(pixelBuffer, withPresentationTime: presentationTime) {
                    throw NSError(domain: "YourAppDomain", code: 3, userInfo: [NSLocalizedDescriptionKey: "Error appending pixel buffer"])
                }
                
                frameCount += 1
            }
        }
        
        videoWriterInput.markAsFinished()
        await videoWriter.finishWriting()
        
        if let error = videoWriter.error {
            throw error
        }
    }

    private func addExifMetadataToImage(at imagePath: URL, orientation: CGImagePropertyOrientation) throws {
        let source = CGImageSourceCreateWithURL(imagePath as CFURL, nil)
        guard let source = source else {
            throw NSError(domain: "AddExifError", code: 1, userInfo: [NSLocalizedDescriptionKey: "Failed to load image source"])
        }

        // Get existing metadata
        let metadata = CGImageSourceCopyPropertiesAtIndex(source, 0, nil) as? [CFString: Any] ?? [:]
        var mutableMetadata = metadata
        
        // Add/Update the image orientation in metadata
        mutableMetadata[kCGImagePropertyOrientation] = orientation.rawValue

        // Create a destination to save the updated image
        guard let destination = CGImageDestinationCreateWithURL(imagePath as CFURL, UTType.jpeg.identifier as CFString, 1, nil) else {
            throw NSError(domain: "AddExifError", code: 2, userInfo: [NSLocalizedDescriptionKey: "Failed to create image destination"])
        }

        // Add the image to the destination with the updated metadata
        CGImageDestinationAddImageFromSource(destination, source, 0, mutableMetadata as CFDictionary)

        // Finalize the write
        if !CGImageDestinationFinalize(destination) {
            throw NSError(domain: "AddExifError", code: 3, userInfo: [NSLocalizedDescriptionKey: "Failed to write updated image to disk"])
        }
    }
    
}

// MARK: Extension UIImage
extension UIImage {
    func pixelBuffer(width: Int, height: Int) -> CVPixelBuffer? {
        var pixelBuffer: CVPixelBuffer?
        let options: [String: Any] = [
            kCVPixelBufferCGImageCompatibilityKey as String: true,
            kCVPixelBufferCGBitmapContextCompatibilityKey as String: true,
        ]
        let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                         width,
                                         height,
                                         kCVPixelFormatType_32ARGB,
                                         options as CFDictionary,
                                         &pixelBuffer)
        
        guard status == kCVReturnSuccess, let buffer = pixelBuffer else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(buffer, [])
        defer { CVPixelBufferUnlockBaseAddress(buffer, []) }
        
        let context = CGContext(data: CVPixelBufferGetBaseAddress(buffer),
                                width: width,
                                height: height,
                                bitsPerComponent: 8,
                                bytesPerRow: CVPixelBufferGetBytesPerRow(buffer),
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        
        guard let cgImage = self.cgImage, let cgContext = context else {
            return nil
        }
        
        cgContext.draw(cgImage, in: CGRect(origin: .zero, size: size))
        return pixelBuffer
    }
}
